import AnimalComponent from "./components/AnimalComponent";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <AnimalComponent />
    </div>
  );
}

export default App;
