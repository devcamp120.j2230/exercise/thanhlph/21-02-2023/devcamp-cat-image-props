import { Component } from "react";

class InputMessage extends Component {
    onInputChangeHandler = (event) => {
        const { inputMessageChangeHandlerProps } = this.props;
        inputMessageChangeHandlerProps(event.target.value);
    }
    render() {
        const { inputMessageProps } = this.props;
        return (
            <>
                <div>
                    <input style={{ display: "flex", margin: "auto", marginTop: "20px" }} placeholder="write your kind pet in here" onChange={this.onInputChangeHandler} value={inputMessageProps}></input>
                </div>
            </>
        )
    }
}

export default InputMessage;