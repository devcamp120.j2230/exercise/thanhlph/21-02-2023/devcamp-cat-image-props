import { Component } from "react";

class OutputMessage extends Component {
    render() {
        const {outputMessageProps, catImgProps } = this.props;
        return(
            <div style={{marginTop:"5px"}}>
                {outputMessageProps ? <p style={{textAlign:"center", color:"blue"}}>meow not found</p> : null}
                {catImgProps ? <img alt="ImgCat" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/541117/cat.jpg" style={{width:"100",display:"flex",margin:"auto"}} /> : null}
            </div>
        )
    }
}

export default OutputMessage;